const PokemonList = require('../pokemon.js').pokemonlist;
const expect = require('chai').expect;

/*
* Аналогично проверкам на Pokemon.show()
* */

describe('Тесты на метод add класса PokemonList', () => {

    let pokemonlist;

    before( () => {
        pokemonlist = new PokemonList();
    });

    it('Передано имя в кавычках, уровень числом', () => {
        pokemonlist.add('Gaini', 100500);
        expect(pokemonlist).to.not.deep.include([{
            name: 'Gaini',
            level: 100500
        }]);
    });

    it('Передано имя и уровень в кавычках', () => {
        pokemonlist.add('Gaini', '100500');
        expect(pokemonlist).to.not.deep.include([{
            name: 'Gaini',
            level: '100500'
        }]);
    });

    /*
    * Проверка не будет пройдена, из-за имени без ковычек
    * */
    it('Имя передано без кавычек - ошибка', () => {
        pokemonlist.add(Gaini, 100500);

    });

    it('Ничего не передано', () => {
        pokemonlist.add();
    });

    it('Передано пустое имя', () => {
        pokemonlist.add('', 100500);
        expect(pokemonlist).to.not.deep.include([{
            name: '',
            level: '100500'
        }]);
    });

    it('Передан пустой уровень', () => {
        pokemonlist.add('Gaini', '');
        expect(pokemonlist).to.not.deep.include([{
            name: 'Gaini',
            level: ''
        }]);
    });

    it('Передан один аргумент', () => {
        pokemonlist.add('Gaini');
        expect(pokemonlist).to.not.deep.include([{
            name: 'Gaini'
        }]);
    });

});
