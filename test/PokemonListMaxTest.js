const PokemonList = require('../pokemon.js').pokemonlist;
const should = require('chai').should();

describe('Тесты на метод max класса PokemonList', () => {

    let pokemonList, pokemonListWithoutMax, pokemonListEmpty;

    before( () => {
        pokemonList = new PokemonList();
        pokemonList.add('pokemon1', 100);
        pokemonList.add('pokemon2', 200);
        pokemonList.add('pokemon3', 300);
        pokemonList.add('pokemon4', 400);
        pokemonList.add('pokemon5', 500);

        pokemonListWithoutMax = new PokemonList();
        pokemonListWithoutMax.add('pokemon1', 100);
        pokemonListWithoutMax.add('pokemon2', 100);
        pokemonListWithoutMax.add('pokemon3', 100);
        pokemonListWithoutMax.add('pokemon4', 100);
        pokemonListWithoutMax.add('pokemon5', 100);

        pokemonListEmpty = new PokemonList();
    });

    it('Метод Max у нормальной выборки', () => {
        const resultMax = pokemonList.max();
        console.log(resultMax);
        resultMax.should.be.a('object');
        resultMax.name.should.be.a('string');
        resultMax.level.should.be.a('number');
        console.log(resultMax);
    });

    it('Метод Max у выборки без максимума', () => {
        const resultWithoutMax = pokemonListWithoutMax.max();
        resultMax.should.be.a('object');
        resultMax.name.should.be.a('string');
        resultMax.level.should.be.a('number');
        console.log(resultWithoutMax);
    });

    it('Метод Max у пустой выборки', () => {
        const resultEmpty = pokemonListEmpty.max();
        console.log(resultEmpty);
    });

});