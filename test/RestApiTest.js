var chai = require('chai');
var should = require('chai').should();
var chaiHttp = require('chai-http');
var app = require('../app');

chai.use(chaiHttp);

describe('App', function () {
    it('create POST', function (done) {
        chai.request(app)
            .post('/api/v1/users')
            .type('form')
            .send({
                '_method': 'post',
                'id': '10',
                'name': 'name10',
                'score': '1010'
            })
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('object');
                console.log(res.body);
                done();
            });
    });

    it('delete POST', function (done) {
        chai.request(app)
            .delete('/api/v1/users/1')
            .type('form')
            .send({
                '_method': 'delete',
            })
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array', 'object');
                console.log(res.body);
                done();
            });
    });
});