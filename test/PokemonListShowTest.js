const PokemonList = require('../pokemon.js').pokemonlist;

describe('Тесты на метод show класса PokemonList', () => {

    let pokemonlist;

    before( () => {
        pokemonlist = new PokemonList();
        pokemonlist.add('pokemon1', 100);
        pokemonlist.add('pokemon2', 200);
        pokemonlist.add('pokemon3', 300);
        pokemonlist.add('pokemon4', 400);
        pokemonlist.add('pokemon5', 500);
    });

    it('Метод показать работает', () => {
        pokemonlist.show()
    });

});
