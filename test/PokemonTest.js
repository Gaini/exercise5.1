const Pokemon = require('../pokemon.js').pokemon;
const assert = require('assert');
const should = require('chai').should();
const sinon = require('sinon');

describe('Тесты на метод show класса Pokemon', () => {

    let spy;

    before(() => {
        "use strict";
        spy = sinon.spy(console, 'log');
    });

    it('Передано имя в кавычках, уровень числом', () => {
        const name = 'Gaini',
              level = 100500;

        name.should.be.a('string');
        level.should.be.a('number');

        const pokemon = new Pokemon(name, level);
        pokemon.show();
        assert(spy.calledWith('Hi! My name is Gaini, my level is 100500'));
    });

    it('Передано имя и уровень в кавычках', () => {
        const name = 'Gaini',
            level = '100500';

        name.should.be.a('string');
        level.should.be.a('string');

        const pokemon = new Pokemon(name, level);
        pokemon.show();
        assert(spy.calledWith('Hi! My name is Gaini, my level is 100500'));
    });

    /*
     * Проверка не будет пройдена
     * */
    it('Имя передано не строкой - ошибка', () => {
        const name = 123,
            level = 100500;

        name.should.be.a('string');
        level.should.be.a('number');

        const pokemon = new Pokemon(name, level);
        pokemon.show();
    });

    it('Ничего не передано', () => {
        const pokemon = new Pokemon();

        pokemon.show();
        assert(spy.calledWith('Hi! My name is undefined, my level is undefined'));
    });

    it('Передано пустое имя', () => {
        const name = '',
            level = 100500;

        name.should.be.lengthOf(0);
        level.should.be.a('number');

        const pokemon = new Pokemon(name, level);
        pokemon.show();
        assert(spy.calledWith('Hi! My name is , my level is 100500'));
    });

    it('Передан пустой уровень', () => {
        const name = 'Gaini',
            level = null;

        name.should.be.a('string');
        should.equal(level, null);

        const pokemon = new Pokemon(name, level);
        pokemon.show();
        assert(spy.calledWith('Hi! My name is Gaini, my level is null'));
    });

    it('Передан один аргумент', () => {
        const name = 'Gaini';

        name.should.be.a('string');

        const pokemon = new Pokemon(name);
        pokemon.show();
        assert(spy.calledWith('Hi! My name is Gaini, my level is undefined'));
    });
});
